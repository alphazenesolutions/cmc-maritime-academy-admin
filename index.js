const express = require('express')
const path = require('path')
const port = process.env.PORT || 4004
var app = express()
var firebase = require("./database/firebase")
var dotenv = require("dotenv").config()
var db = firebase.firestore();
//view engine
app.set('view engine', 'ejs')

//static path
app.use("/views", express.static(path.join(__dirname + "/views")))
app.use("/assets", express.static(path.join(__dirname + "/assets")))


//body parser
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.get("/", (req, res) => {
    res.render("index")
})
app.get("/home", (req, res) => {
    res.render("dashboard")
})
app.get("/newcourse", (req, res) => {
    res.render("newcourse")
})
app.get("/users", (req, res) => {
    res.render("users")
})
app.get("/feedback", (req, res) => {
    res.render("feedback")
})
app.get("/enquiry", (req, res) => {
    res.render("enquiry")
})
app.get("/subcourse", (req, res) => {
    res.render("subcourse")
})
app.get("/subcourseview", (req, res) => {
    res.render("subcourseview")
})
app.get("/purchasehistory", (req, res) => {
    res.render("purchasehistory")
})
app.get("/faculty", (req, res) => {
    res.render("faculty")
})
app.get("/gallery", (req, res) => {
    res.render("gallery")
})
app.get("/certificateupload", (req, res) => {
    res.render("certificateupload")
})
app.get("/timetablecreate", (req, res) => {
    res.render("timetablecreate")
})
app.get("/timetable", (req, res) => {
    res.render("timetable")
})
app.get("/timetableview", (req, res) => {
    res.render("timetableview")
})
app.get("/pdf", (req, res) => {
    res.render("pdf")
})
app.get("/discount", (req, res) => {
    res.render("discount")
})
app.get("/category", (req, res) => {
    res.render("category")
})
app.get("/courses", (req, res) => {
    res.render("courses")
})
app.get("/brochure", (req, res) => {
    res.render("brochure")
})
app.get("/pendingapplication", (req, res) => {
    res.render("pendingapplication")
})
app.get("/add", (req, res) => {
    res.render("addaccountant")
})
app.get("/addadmin", (req, res) => {
    res.render("addadmin")
})
app.get("/usersdisplay", (req, res) => {
    res.render("uersdisplay")
})
app.get("/announcement", (req, res) => {
    res.render("announcement")
})
app.get("/testimonial", (req, res) => {
    res.render("testimonial")
})
app.get("/managebatch", (req, res) => {
    res.render("managebatch")
})
app.get("/application", (req, res) => {
    res.render("application")
})


//Accountant Router
app.get("/acchome", (req, res) => {
    res.render("accdashboard")
})
app.get("/accusers", (req, res) => {
    res.render("accusers")
})
app.get("/accpendingusers", (req, res) => {
    res.render("accpendingusers")
})
app.get("/acccompltedcoursehistory", (req, res) => {
    res.render("acccompltedcoursehistory")
})
app.get("/accupcommingcoursehistory", (req, res) => {
    res.render("accupcommingcoursehistory")
})

var nodemailer = require('nodemailer')


var smtpTransport = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'noreply@cmcacademy.ac.in',
        pass: 'welcome@2022' // naturally, replace both with your real credentials or an application-specific password
    }
});

app.post('/timetablesend', async (req, res) => {
    mailOptions = {
        from: process.env.EMAIL_USER,
        to: req.body.email,
        subject: "Time Table",
        text: "Hello world?",
        html: req.body.mailtemplate,
    }
    smtpTransport.sendMail(mailOptions, function (error, response) {
        if (error) {
            console.log(error);
            return res.send(false)
        } else {
            console.log(response)
            return res.send(true)

        }
    });
})

app.post('/certificate', async (req, res) => {
    const mailOptions = {
        from: 'noreply@cmcacademy.ac.in',
        to:  req.body.email,
        subject: 'Course Certificate..',
        html: req.body.mailtemplate
    };

    smtpTransport.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            return res.send(true)
        }
    });
})

app.listen(port, () => { console.log(`App Running on http://localhost:${port}`) })