var db = firebase.firestore()
var userid = sessionStorage.getItem("userapplication")
var name = []
db.collection("users").doc(userid).get().then((doc) => {
    document.getElementById("username").innerHTML = `${doc.data().firstname == undefined ? "" : `${doc.data().firstname} ${doc.data().lastname}`}`
    document.getElementById("dateofbirth").innerHTML = `${doc.data().dob == undefined ? "" : doc.data().dob}`
    document.getElementById("phonenumber").innerHTML = `${doc.data().phonenumber == undefined ? "" : doc.data().phonenumber}`
    document.getElementById("email").innerHTML = `${doc.data().email == undefined ? "" : doc.data().email}`
    document.getElementById("place").innerHTML = `${doc.data().place == undefined ? "" : doc.data().place}`
    document.getElementById("address").innerHTML = `${doc.data().address == undefined ? "" : doc.data().address}`
    document.getElementById("passport").innerHTML = `${doc.data().passport == undefined ? "" : doc.data().passport}`
    document.getElementById("indosnumber").innerHTML = `${doc.data().indosnumber == undefined ? "" : doc.data().indosnumber}`
    document.getElementById("discharge").innerHTML = `${doc.data().discharge == undefined ? "" : doc.data().discharge}`
    document.getElementById("grade").innerHTML = `${doc.data().grade == undefined ? "" : doc.data().grade}`
    document.getElementById("gradenumber").innerHTML = `${doc.data().gradenumber == undefined ? "" : doc.data().gradenumber}`
    document.getElementById("years").innerHTML = `${doc.data().year == undefined ? "" : doc.data().year}`
    document.getElementById("month").innerHTML = `${doc.data().month == undefined ? "" : doc.data().month}`
    document.getElementById("day").innerHTML = `${doc.data().day == undefined ? "" : doc.data().day}`
    if (doc.data().profilepic !== undefined) {
        document.getElementById("profilepic").innerHTML = `<img src="${doc.data().profilepic}" id="dashdppicnew" alt="" style="width: 100px;height: 100px;"
        srcset="">`
    }else{
        document.getElementById("profilepic").innerHTML = `<img src="/assets/images/profile_av.jpg" id="dashdppicnew" alt="" style="width: 100px;height: 100px;"
        srcset="">`
    }
    sessionStorage.setItem("username", `${doc.data().firstname} ${doc.data().lastname}`)

})

function generatePDF() {
    const invoice = this.document.getElementById("test");
    var username = sessionStorage.getItem("username")
    console.log(username)
    var opt = {
        margin: 0.5,
        filename: `${username}.pdf`,
        image: { type: 'jpeg', quality: 1 },
        html2canvas: { scale: 1 },
        jsPDF: { unit: 'in', format: 'letter', orientation: 'portrait' }
    };
    html2pdf().from(invoice).set(opt).save();
}



db.collection("purchasecourses").where("userid", "==", userid).get().then(async (snap) => {
    snap.forEach((doc) => {
        db.collection("newcourse").doc(doc.data().coureseid).get().then(async (docs) => {
            if (docs.data() != undefined) {
               console.log(docs.data().title)
               document.getElementById("courseitem").innerHTML+=`${docs.data().title}, `
            }
            

        })
    })
})