var db = firebase.firestore();

document.getElementById("newccatbtn").addEventListener("click", async () => {
    var category = document.getElementById("categorymain").value
    document.getElementById("newccatbtn").innerHTML = " <span class='spinner-border spinner-border-sm mr-2' role='status'style='padding: 7px;' aria-hidden='true'></span>Please Wait..."
    db.collection("category").doc().set({
        category: category
    }).then(() => {
        toastr["success"]("New Category Added Successfully..");
        setTimeout(() => { window.location.reload() }, 2000)
    }).catch((error) => {
        console.log(error.message)
        toastr["error"](error.message);
    })
})

db.collection("category").get().then((snap) => {
    var categorydata = []
    snap.forEach((doc) => {
        categorydata.push(doc.data())
        if(doc.data().category !="All"){
            document.getElementById("category").innerHTML += "<tr class='newcourse1'><td>" + doc.data().category + "</td><td>" + "<button class='btn btn-danger' id=" + doc.id + " onclick='del(this.id)'><i class='fa fa-trash-o aria-hidden='true></i> Delete</button> <button class='btn btn-info' id=" + doc.id + " data-bs-toggle='modal' data-bs-target='#exampleModaledit' onclick='view(this.id)'><i class='fa fa-file-o aria-hidden='true></i> View Course</button>" + "</td></tr>"
        }
      
    })
    if (categorydata.length != 0) {

        document.getElementById("coursenoti").style.display = "none"
    } else {
        document.getElementById("coursenoti").style.display = "block"
    }
})
document.getElementById("usersearch").addEventListener("keyup", async () => {
    document.getElementById("category").innerHTML = ""
    var searchname = document.getElementById("usersearch").value.toLowerCase()
    db.collection("category").get().then((snap) => {
        var filterdoctor = []
        snap.forEach((doc) => {
            if (doc.data().category.toLowerCase().includes(searchname)) {
                filterdoctor.push(doc.data())
            }
        })
        for (var i = 0; i < filterdoctor.length; i++) {
            document.getElementById("category").innerHTML += "<tr class='newcourse1'><td>" + filterdoctor[i].category + "</td><td>" + "<button class='btn btn-danger' id=" + filterdoctor[i].id + " onclick='del(this.id)'><i class='fa fa-trash-o aria-hidden='true></i> Delete</button> <button class='btn btn-info' id=" + filterdoctor[i].id + " data-bs-toggle='modal' data-bs-target='#exampleModaledit' onclick='view(this.id)' ><i class='fa fa-file-o aria-hidden='true></i> View Course</button>" + "</td></tr>"
        }
        if (filterdoctor.length == 0) {
            document.getElementById("facultynoti").style.display = "block"
        }
    })
})

del = (e) => {
    var alertdata = confirm("Are You Sure Delete Category..");
    if (alertdata === true) {
        db.collection("category").doc(e).get().then((doc) => {
            db.collection("newcourse").where("category", "==", doc.data().category).get().then((snap) => {
                var course = []
                snap.forEach((docs) => {
                    course.push(docs.id)
                })
                if (course.length !== 0) {
                    for (var i = 0; i < course.length; i++) {
                        db.collection("newcourse").doc(course[i]).delete().then(() => {
                            db.collection("category").doc(e).delete().then(() => {
                                toastr["error"]("Category Deleted..");
                                setTimeout(() => { window.location.reload() }, 1000)
                            })
                        })
                    }
                } else {
                    db.collection("category").doc(e).delete().then(() => {
                        toastr["error"]("Category Deleted..");
                        setTimeout(() => { window.location.reload() }, 1000)
                    })
                }


            })

        })
    }


}






view = (e) => {
    db.collection("category").doc(e).get().then((doc) => {
        console.log(doc.data().category)
        sessionStorage.setItem("categoryname", doc.data().category)
    })
    sessionStorage.setItem("categoryid", e)
    setTimeout(() => { window.location.replace("/newcourse") }, 1000)

}