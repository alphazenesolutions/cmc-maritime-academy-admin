var db = firebase.firestore();

document.getElementById("newannouncementbtn").addEventListener("click", async () => {
    var announcement = document.getElementById("announcement").value
    db.collection("announcement").doc("msg").set({
        announcement: announcement
    }).then(() => {
        toastr["success"]("New Announcement Updated Successfully..");
        setTimeout(() => { window.location.reload() }, 2000)
    })
})

db.collection("announcement").doc("msg").get().then((doc) => {
    document.getElementById("announcement").value = doc.data().announcement
})