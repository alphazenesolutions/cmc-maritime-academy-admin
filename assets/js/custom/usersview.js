var db = firebase.firestore();
var userid = localStorage.getItem("useridview");
db.collection("users")
  .doc(userid)
  .get()
  .then((doc) => {
    if (doc.data() != undefined) {
      console.log(doc.data(), "doc.data()");
      document.getElementById("fullname").innerHTML = `${
        doc.data().firstname == "" ? "User Not Provide" : doc.data().firstname
      } ${doc.data().lastname}`;
      document.getElementById("email").innerHTML = `${
        doc.data().email == "" ? "User Not Provide" : doc.data().email
      }`;
      console.log(doc.data().passport, "doc.data().bsidnumber");
      document.getElementById("BSIDnumber").innerHTML = `${
        doc.data().bsidnumber !== undefined
          ? doc.data().bsidnumber
          : "User Not Provide"
      }`;
      document.getElementById("phonenumber").innerHTML = `${
        doc.data().phonenumber == ""
          ? "User Not Provide"
          : doc.data().phonenumber
      }`;
      document.getElementById("address").innerHTML = `${
        doc.data().address == "" ? "User Not Provide" : doc.data().address
      }`;
      document.getElementById("date").innerHTML = `${
        doc.data().dob == "" ? "User Not Provide" : doc.data().dob
      }`;
      document.getElementById("indosnumber").innerHTML = `${
        doc.data().indosnumber == ""
          ? "User Not Provide"
          : doc.data().indosnumber
      }`;
      document.getElementById("dob").innerHTML = `${
        doc.data().dob == "" ? "User Not Provide" : doc.data().dob
      }`;
      document.getElementById("passportnumber").innerHTML = `${
        doc.data().passport == "" ? "User Not Provide" : doc.data().passport
      }`;
      document.getElementById("dischargenumbernew").innerHTML = `${
        doc.data().discharge == "" ? "User Not Provide" : doc.data().discharge
      }`;

      document.getElementById("urls").innerHTML += `
        ${
          doc.data().dischargeurl == null
            ? ""
            : `<td>Discharge Certificate - </td>
        <tr><a href="${
          doc.data().dischargeurl == null
            ? (href = "javascript:void(0)")
            : doc.data().dischargeurl
        }" target="__blank">Download</a></tr>
        <br>`
        }
        ${
          doc.data().indosurl == null
            ? ""
            : `<td>Indos Number Certificate - </td>
        <tr><a href="${
          doc.data().indosurl == null
            ? (href = "javascript:void(0)")
            : doc.data().indosurl
        }" target="__blank">Download</a></tr>
        <br>`
        }    
        ${
          doc.data().medicalurl == null
            ? ""
            : `<td>Medical fitness Certificate - </td>
        <tr><a href="${
          doc.data().medicalurl == null
            ? (href = "javascript:void(0)")
            : doc.data().medicalurl
        }" target="__blank">Download</a></tr>
        <br>`
        }
        ${
          doc.data().signatureurl == null
            ? ""
            : ` <td>Signature  - </td>
        <tr><a href="${
          doc.data().signatureurl == null
            ? (href = "javascript:void(0)")
            : doc.data().signatureurl
        }" target="__blank">Download</a></tr> <br>`
        }   
       
        ${
          doc.data().gradeimgurl == null
            ? ""
            : ` <td>Grade  - </td>
        <tr><a href="${
          doc.data().gradeimgurl == null
            ? (href = "javascript:void(0)")
            : doc.data().gradeimgurl
        }" target="__blank">Download</a></tr> <br>`
        }   
               
        ${
          doc.data().certificateurl == null
            ? ""
            : ` <td>Certificate  - </td>
        <tr><a href="${
          doc.data().certificateurl == null
            ? (href = "javascript:void(0)")
            : doc.data().certificateurl
        }" target="__blank">Download</a></tr> <br>`
        }   

        ${
          doc.data().passportimgurl == null
            ? ""
            : ` <td>Passport  - </td>
        <tr><a href="${
          doc.data().passportimgurl == null
            ? (href = "javascript:void(0)")
            : doc.data().passportimgurl
        }" target="__blank">Download</a></tr> <br>`
        }   
        <a href="javascript:void(0)" id="${
          doc.data().clientid
        }" onclick="viewapplication(this.id)">View Application</a>
        `;
      //
    }
  });

viewapplication = (e) => {
  sessionStorage.setItem("userapplication", e);
  window.open("/application");
};

db.collection("purchasecourses")
  .where("userid", "==", userid)
  .get()
  .then((snap) => {
    snap.forEach((doc) => {
      db.collection("newcourse")
        .doc(doc.data().coureseid)
        .get()
        .then((docs) => {
          if (docs.data() != undefined) {
            // for (var i = 0; i < docs.data().startdate.length; i++) {
            var today = moment().format("YYYY-MM-DD");

            if (today < doc.data().batchdate) {
              if (docs.data() == undefined) {
                document.getElementById("nocourse").style.display = "block";
              } else {
                document.getElementById("upcomming").innerHTML += `
                     
                            <li class="dd-item" data-id="2">
                            <div
                                class="dd-handle d-flex justify-content-between align-items-center">
                                <div class="h6 mb-0">${
                                  docs.data().title
                                }</div>  
                            </div>
                            <div class="dd-content p-15">
                                <ul class="list-unstyled d-flex bd-highlight align-items-center">
                                    <li class="mr-2 flex-grow-1 bd-highlight"><span
                                            class="badge badge-default"><i
                                                class="zmdi zmdi-time"></i> ${
                                                  doc.data().batchdate
                                                }</span></li>
                                    <li class="mr-2 flex-grow-1 bd-highlight"><span
                                            class="badge badge-default"><i
                                                class="zmdi zmdi-time"></i> ${
                                                  doc.data().batch
                                                }</span></li>
                                                <li class="mr-2 flex-grow-1 bd-highlight"><button class="btn btn-danger" id="${
                                                  doc.id
                                                }" onclick="removeitempurchase(this)">Delete</button></li>
                                </ul>
                            </div>
                        </li>
        `;
              }
            }
            // }
          }
        });
    });
  });

db.collection("purchasecourses")
  .where("userid", "==", userid)
  .get()
  .then((snap) => {
    snap.forEach((doc) => {
      db.collection("newcourse")
        .doc(doc.data().coureseid)
        .get()
        .then((docs) => {
          if (docs.data() != undefined) {
            // for (var i = 0; i < docs.data().startdate.length; i++) {
            var today = moment().format("YYYY-MM-DD");
            if (today > doc.data().batchdate) {
              console.log(docs.data().title);
              document.getElementById("completed").innerHTML += `
                                            <li class="dd-item" data-id="2">
                                            <div
                                                class="dd-handle d-flex justify-content-between align-items-center">
                                                <div class="h6 mb-0">${
                                                  docs.data().title
                                                }</div>  
                                            </div>
                                            <div class="dd-content p-15">
                                                <ul class="list-unstyled d-flex bd-highlight align-items-center">
                                                    <li class="mr-2 flex-grow-1 bd-highlight"><span
                                                            class="badge badge-default"><i
                                                                class="zmdi zmdi-time"></i> ${
                                                                  doc.data()
                                                                    .batchdate
                                                                }</span></li>
                                                    <li class="mr-2 flex-grow-1 bd-highlight"><span
                                                            class="badge badge-default"><i
                                                                class="zmdi zmdi-time"></i> ${
                                                                  doc.data()
                                                                    .batch
                                                                }</span></li>
                                                                <li class="mr-2 flex-grow-1 bd-highlight"><button class="btn btn-danger" id="${
                                                                  doc.id
                                                                }" onclick="removeitempurchase(this)">Delete</button></li>
                                                                </ul>
                                                                
                                            </div>
                                        </li>
                        `;
            }
            // }
          }
        });
    });
  });

function addofflineupcommingcourse() {
  var title = document.getElementById("title").value;
  var batchdate = document.getElementById("batchdate").value;
  var batchno = document.getElementById("batchno").value;
  db.collection("offlinecourse")
    .doc()
    .set({
      batch: batchno,
      date: batchdate,
      title: title,
      userid: localStorage.getItem("useridview"),
    })
    .then(() => {
      toastr["success"]("New Course Added Successfully..");
      setTimeout(() => {
        window.location.reload();
      }, 2000);
    });
}

db.collection("offlinecourse")
  .where("userid", "==", userid)
  .get()
  .then((snap) => {
    snap.forEach((doc) => {
      var today = moment().format("YYYY-MM-DD");
      if (today > doc.data().date) {
        document.getElementById("completed").innerHTML += `
                                <li class="dd-item" data-id="2">
                                <div
                                    class="dd-handle d-flex justify-content-between align-items-center">
                                    <div class="h6 mb-0">${
                                      doc.data().title
                                    }</div>  
                                </div>
                                <div class="dd-content p-15">
                                    <ul class="list-unstyled d-flex bd-highlight align-items-center">
                                        <li class="mr-2 flex-grow-1 bd-highlight"><span
                                                class="badge badge-default"><i
                                                    class="zmdi zmdi-time"></i> ${
                                                      doc.data().date
                                                    }</span></li>
                                        <li class="mr-2 flex-grow-1 bd-highlight"><span
                                                class="badge badge-default"><i
                                                    class="zmdi zmdi-time"></i> ${
                                                      doc.data().batch
                                                    }</span></li>
                                        <li class="mr-2 flex-grow-1 bd-highlight"><button class="btn btn-danger" id="${
                                          doc.id
                                        }" onclick="removeitem(this)">Delete</button></li>
                                    </ul>
                                </div>
                               
                            </li>
            `;
      }
    });
  });

removeitem = (e) => {
  db.collection("offlinecourse")
    .doc(e.id)
    .delete()
    .then(() => {
      toastr["error"]("Course Deleted..");
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    });
};

removeitempurchase = (e) => {
  db.collection("purchasecourses")
    .doc(e.id)
    .delete()
    .then(() => {
      toastr["error"]("Course Deleted..");
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    });
};

db.collection("offlinecourse")
  .where("userid", "==", userid)
  .get()
  .then((snap) => {
    snap.forEach((doc) => {
      var today = moment().format("YYYY-MM-DD");
      if (today < doc.data().date) {
        console.log(doc.data().date);
        document.getElementById("upcomming").innerHTML += `
                                <li class="dd-item" data-id="2">
                                <div
                                    class="dd-handle d-flex justify-content-between align-items-center">
                                    <div class="h6 mb-0">${
                                      doc.data().title
                                    }</div>  
                                </div>
                                <div class="dd-content p-15">
                                    <ul class="list-unstyled d-flex bd-highlight align-items-center">
                                        <li class="mr-2 flex-grow-1 bd-highlight"><span
                                                class="badge badge-default"><i
                                                    class="zmdi zmdi-time"></i> ${
                                                      doc.data().date
                                                    }</span></li>
                                        <li class="mr-2 flex-grow-1 bd-highlight"><span
                                                class="badge badge-default"><i
                                                    class="zmdi zmdi-time"></i>${
                                                      doc.data().batch
                                                    }</span></li>
                                                    <li class="mr-2 flex-grow-1 bd-highlight"><button class="btn btn-danger" id="${
                                                      doc.id
                                                    }" onclick="removeitem(this)">Delete</button></li>        
                                    </ul>
                                </div>
                            </li>
            `;
      }
    });
  });

document.getElementById("title").addEventListener("keyup", async () => {
  document.getElementById("courses").innerHTML = "";
  var searchname = document.getElementById("title").value.toLowerCase();
  db.collection("newcourse")
    .get()
    .then((snap) => {
      var filterdoctor = [],
        filterid = [];
      snap.forEach((doc) => {
        if (doc.data().title.toLowerCase().includes(searchname)) {
          filterdoctor.push(doc.data());
          filterid.push(doc.id);
        }
      });
      for (var i = 0; i < filterdoctor.length; i++) {
        console.log(filterdoctor[i], filterid[i]);
        document.getElementById("courses").innerHTML += `
            <span class="pcoded-mtext" id="${filterid[i]}" onclick="courseview(this)">${filterdoctor[i].title}</span><br>
                    `;
      }
      if (filterdoctor.length == 0) {
        document.getElementById("facultynoti").style.display = "block";
      }
    });
});

function courseview(e) {
  document.getElementById("datediv").style.display = "block";
  document.getElementById("batchdiv").style.display = "block";
  db.collection("newcourse")
    .doc(e.id)
    .get()
    .then((doc) => {
      document.getElementById("title").value = doc.data().title;
      for (var i = 0; i < doc.data().startdate.length; i++) {
        var today = moment().format("YYYY-MM-DD");
        if (today < doc.data().startdate[i].date) {
          document.getElementById("batchdate").innerHTML += `
                    <option id="${[i]}" onclick="batchdatafind(this)">${
            doc.data().startdate[i].date
          }</option>
            `;
          document.getElementById("batchno").innerHTML += `
                <option>${doc.data().startdate[i].batch}</option>
                `;
        }
      }
    });
}

function batchdatafind(e) {
  alert("okk");
  console.log(e);
}
