var db = firebase.firestore();

document.getElementById("newbrochurebtn").addEventListener("click", async () => {
    var brochure = document.getElementById("brochure").files
    console.log(brochure)
    document.getElementById("newbrochurebtn").innerHTML = " <span class='spinner-border spinner-border-sm mr-2' role='status'style='padding: 7px;' aria-hidden='true'></span>Please Wait..."
    let file11 = new Promise((resolve, reject) => {
        var storageRef = firebase.storage().ref("brochure/" + brochure[0].name);
        storageRef.put(brochure[0]).then(function (snapshot) {
            storageRef.getDownloadURL().then(function (url) {  //img download link ah ketakiradhu
                setTimeout(() => resolve(url), 1000)
            })
        });

    });
    var imgurl = await file11
    console.log(imgurl)
    db.collection("brochure").doc().set({
        brochure: imgurl
    }).then(() => {
        toastr["success"]("New Brochure Added Successfully..");
        setTimeout(() => { window.location.reload() }, 2000)
    }).catch((error) => {
        console.log(error.message)
        toastr["error"](error.message);
    })
})

db.collection("brochure").get().then((snap) => {
    var brochuredata = []
    snap.forEach((doc) => {
        brochuredata.push(doc.data())
        document.getElementById("brochuredata").innerHTML += "<tr class='newcourse1'><td>" + "<button class='btn btn-danger' id=" + doc.id + " onclick='del(this.id)'><i class='fa fa-trash-o aria-hidden='true></i> Delete</button> <button class='btn btn-info' id=" + doc.data().brochure + " data-bs-toggle='modal' data-bs-target='#exampleModaledit' onclick='view(this.id)'><i class='fa fa-file-o aria-hidden='true></i> View</button>" + "</td></tr>"
    })
    if (brochuredata.length != 0) {

        document.getElementById("coursenoti").style.display = "none"
        document.getElementById("inputbtnbrochur").style.display = "none"
    } else {
        document.getElementById("coursenoti").style.display = "block"
    }
})

del = (e) => {
    db.collection("brochure").doc(e).delete().then(() => {
        toastr["error"]("Brochure Deleted..");
        setTimeout(() => { window.location.reload() }, 1000)
    })
}

view = (e) => {
    console.log(e)
    window.open(e, '_blank');

    // db.collection("brochure").doc(e).get().then((doc) => {
    //     sessionStorage.setItem("categoryname", doc.data().category)
    // })
    // sessionStorage.setItem("categoryid", e)
    // setTimeout(() => { window.location.replace("/newcourse") }, 1000)

}