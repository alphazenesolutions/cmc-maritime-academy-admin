var db = firebase.firestore();

db.collection("enquire").get().then((snap)=>{
    snap.forEach((doc)=>{
        
            document.getElementById("users").innerHTML +=`
            <tr class='newenquiry1'>
                <td>${doc.data().name}</td>
                <td>${doc.data().message}</td>
                <td>${doc.data().email}</td>
                <td>${doc.data().phone==undefined?"-":doc.data().phone}</td>
                <td>${doc.data().date}</td>
                <td><button class='btn btn-primary' id="${doc.id}" onclick='del(this.id)'><i class='fa fa-check aria-hidden='true></i> Click For Solved</button></td>
            </tr>
            `
  
        })
})

del = (e) => {

    db.collection("enquire").doc(e).delete().then(() => {
        // toastr["error"]("Course Deleted");
        setTimeout(() => { window.location.reload() }, 1000)
    })

}


document.getElementById("usersearch").addEventListener("keyup", async () => {
    document.getElementById("users").innerHTML = ""
    var searchname = document.getElementById("usersearch").value.toLowerCase()
    db.collection("enquire").get().then((snap) => {
            var filterdoctor = []
            snap.forEach((doc) => {
                if (doc.data().name.toLowerCase().includes(searchname) ) {
                    filterdoctor.push(doc.data())
                }
            })
           for(var i=0;i<filterdoctor.length;i++){
               document.getElementById("users").innerHTML += `
            <tr>
                <td>${filterdoctor[i].name}</td>
                <td>${filterdoctor[i].message}</td>
                <td>${filterdoctor[i].email}</td>
                <td>${filterdoctor[i].phone}</td>
                <td>${filterdoctor[i].date}</td>
                <td><button class='btn btn-danger' disabled id="${filterdoctor[i]}" onclick='del(this.id)'><i class='fa fa-trash-o aria-hidden='true></i> Delete</button> <button disabled class='btn btn-info' id="${filterdoctor[i]}" data-bs-toggle='modal' data-bs-target='#exampleModaledit' onclick='edit(this.id)'><i class='fa fa-pencil aria-hidden='true></i> Edit</button></td>
            </tr>
            `
           }
        })   
})