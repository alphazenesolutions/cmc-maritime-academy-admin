var db = firebase.firestore();
var courseid = sessionStorage.getItem("courseid")

db.collection("timetablefile").get().then((snap) => {
    var data=[]
    snap.forEach((doc) => {
        if (doc.data() != undefined) {
            if (doc.data().courseid == courseid) {
                data.push(doc.id)
                document.getElementById("timetable").innerHTML +=
                    `
                <tr class='newcourse1'>
                <td>Batch : ${doc.data().batch}</td>
               <td> <button class='btn btn-info'  ><i class='fa fa-file-o aria-hidden='true></i><a style="text-decoration: none;color: white;" target='_blank' class='mr-2' href='${doc.data().timetable}' >View</a></button>
                <button class='btn btn-danger' id="${doc.id}" onclick='del(this.id)' ><i class='fa  fa-trash aria-hidden='true></i>Delete</button></td>
                </tr>
                `
            }
        }
    })
    if(data.length==0){
        document.getElementById("nodata").style.display="block"
    }else{
        document.getElementById("nodata").style.display="none"
    }
})

del = (e) => {
    db.collection("timetablefile").doc(e).delete().then(() => {
         alert("Time Table Deleted")
         window.location.reload()
    })
}