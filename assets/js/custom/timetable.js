var db = firebase.firestore();

db.collection("newcourse").get().then((snap) => {
    var data = []
    snap.forEach((doc) => {
        data.push(doc.data())
        document.getElementById("timetable").innerHTML +=
            `
        <tr class='newcourse1'>
        <td>${doc.data().title}</td>
        <td><button class='btn btn-success' id="${doc.id}" onclick='add(this.id)'><i class='fa fa-file-o aria-hidden='true></i> Add</button><button class='btn btn-danger' id="${doc.id}" onclick='view(this.id)'><i class='fa fa-file-o aria-hidden='true></i> View</button></td>
        </tr>
        `
    })
    if (data.length != 0) {
        if (data.length > 10) {
            var items = $(".timetable .newcourse1");
            var numItems = data.length;

            var perPage = 10;

            items.slice(perPage).hide();

            $('#pagination-container1').pagination({
                items: numItems,
                itemsOnPage: perPage,
                prevText: "&laquo;",
                nextText: "&raquo;",
                onPageClick: function (pageNumber) {
                    var showFrom = perPage * (pageNumber - 1);
                    var showTo = showFrom + perPage;
                    items.hide().slice(showFrom, showTo).show();
                }
            });
        }
        document.getElementById("facultynoti").style.display = "none"
    } else {
        document.getElementById("facultynoti").style.display = "block"
    }
})
{/* <button class='btn btn-info' id="${doc.id}" data-bs-toggle='modal' data-bs-target='#exampleModal' onclick='create(this.id)'><i class='fa fa-pencil aria-hidden='true></i> Create</button> */ }
function create(e) {
    sessionStorage.setItem("courseid", e)
    window.location.replace("/timetablecreate")
}
function view(e) {
    sessionStorage.setItem("courseid", e)
    window.location.replace("/timetableview")
}

async function add(e) {
    var docid = e
    sessionStorage.setItem("courseid", e)
    var batches = []
    await db.collection("newcourse").doc(docid).get().then((doc) => {
        if (doc.data() != undefined) {
            batches.push(...doc.data().startdate)
        }
    })
    document.getElementById("batches").innerHTML = ""
    if (batches.length != 0) {
        batches.forEach((batch) => {
            document.getElementById("batches").innerHTML += `<select value='${batch.batch}' >${batch.batch}</select>`
        })
    }
    $("#sendTimetable").modal("show")
}


document.getElementById("usersearch").addEventListener("keyup", async () => {
    document.getElementById("timetable").innerHTML = ""
    var searchname = document.getElementById("usersearch").value.toLowerCase()
    db.collection("newcourse").get().then((snap) => {
        var filterdoctor = []
        snap.forEach((doc) => {
            if (doc.data().title.toLowerCase().includes(searchname)) {
                filterdoctor.push(doc.data())
            }
        })
        for (var i = 0; i < filterdoctor.length; i++) {
            console.log(filterdoctor[i])
            document.getElementById("timetable").innerHTML += `
            <tr>
                <td>${filterdoctor[i].title}</td>
                <td><button class='btn btn-danger' disabled id="${filterdoctor[i]}" onclick='del(this.id)'><i class='fa fa-trash-o aria-hidden='true></i> Delete</button> <button disabled class='btn btn-info' id="${filterdoctor[i]}" data-bs-toggle='modal' data-bs-target='#exampleModaledit' onclick='edit(this.id)'><i class='fa fa-pencil aria-hidden='true></i> Edit</button></td>
            </tr>
            `
        }
    })


})