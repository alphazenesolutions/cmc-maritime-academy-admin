var db = firebase.firestore();


db.collection("newcourse").get().then(async (snap) => {
    var purchasedata = []
    snap.forEach((doc) => {
        for (var i = 0; i < doc.data().startdate.length; i++) {
            var today = moment().format("YYYY-MM-DD")
            if (today > doc.data().startdate[i].date) {
                purchasedata.push(doc.data())
                console.log(doc.data())
                document.getElementById("purchaseitem").innerHTML += `
                <tr class="newfaculty1">
                    <td>${doc.data().category}</td>
                    <td class="d-none d-md-table-cell">${doc.data().title}</td>
                    <td class="ongoing">${doc.data().subheading}</td>
                    <td>${doc.data().startdate[i].batch}</td>
                    <td>${doc.data().ofees}</td>
                    <td>${moment(doc.data().startdate[i].date).add(10, 'days').format("DD-MM-YYYY")}</td>
                </tr>
                `
            }
        }
       
        if (purchasedata.length == 0) {
            document.getElementById("facultynoti").style.display = "none"
        } else {
            document.getElementById("facultynoti").style.display = "none"
        }
    })
    if (purchasedata.length != 0) {
        var items = $("#purchaseitem .newfaculty1");
        var numItems = purchasedata.length;
        var perPage = 25;
        items.slice(perPage).hide();
        $('#pagination-container1').pagination({
            items: numItems,
            itemsOnPage: perPage,
            prevText: "&laquo;",
            nextText: "&raquo;",
            onPageClick: function (pageNumber) {
                var showFrom = perPage * (pageNumber - 1);
                var showTo = showFrom + perPage;
                items.hide().slice(showFrom, showTo).show();
            }
        });
        document.getElementById("facultynoti").style.display = "none"
    } else {
        document.getElementById("facultynoti").style.display = "none"
    }
   
})