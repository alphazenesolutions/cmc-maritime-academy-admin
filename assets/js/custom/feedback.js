var db = firebase.firestore();

db.collection("feedback").get().then((snap) => {
    snap.forEach((doc) => {
        document.getElementById("users").innerHTML += "<tr class='newcourse1'><td>" + doc.data().name + "</td><td>" + doc.data().feedback
            + "</td><td>" + "<button class='btn btn-primary' id=" + doc.id + " onclick='del(this.id)'><i class='fa fa-check aria-hidden='true></i> Click To Solved</button> " + "</td></tr>"
    })
})

del = (e) => {

    db.collection("feedback").doc(e).delete().then(() => {
        // toastr["error"]("Course Deleted");
        setTimeout(() => { window.location.reload() }, 1000)
    })

}


document.getElementById("usersearch").addEventListener("keyup", async () => {
    document.getElementById("users").innerHTML = ""
    var searchname = document.getElementById("usersearch").value.toLowerCase()
    db.collection("feedback").get().then((snap) => {
        var filterdoctor = []
        snap.forEach((doc) => {
            if (doc.data().name.toLowerCase().includes(searchname)) {
                filterdoctor.push(doc.data())
            }
        })
        for (var i = 0; i < filterdoctor.length; i++) {
            document.getElementById("users").innerHTML += `
            <tr>
                <td>${filterdoctor[i].name}</td>
                <td>${filterdoctor[i].feedback}</td>
                <td><button class='btn btn-primary' disabled id="" onclick='del(this.id)'><i class='fa fa-check aria-hidden='true></i> Click To Solved</button></td>
            </tr>
            `
        }
    })
})