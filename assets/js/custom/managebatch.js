var courseidbatch = sessionStorage.getItem("courseidbatch")
console.log(courseidbatch)
document.getElementById("newbatchaddbtn").addEventListener("click", async () => {
    var tabledata = []
    var data = {}
    tabledata.push(
        data = {
            "batch": document.getElementById("batchnew").value,
            "date": document.getElementById("datenew").value,
            "time": document.getElementById("timenew").value,
            "noof": document.getElementById("noofnew").value,

        }
    )
    var finaldata = []
    await db.collection("newcourse").doc(courseidbatch).get().then(async (doc) => {
        var datalatest = []
        for (var i = 0; i < doc.data().startdate.length; i++) {
            finaldata.push(doc.data().startdate[i])
        }
        if (datalatest.length != 0) {
            finaldata.push(datalatest)
        }
    })
    finaldata.push(tabledata[0])
    db.collection("newcourse").doc(courseidbatch).update({
        startdate: finaldata,
    }).then((doc) => {
        toastr["success"]("Batch Updated Successfully..");
        setTimeout(() => { window.location.reload() }, 1000)
        // document.getElementById("enewcoursebtn").innerHTML = "Update"
    })
})

db.collection("newcourse").doc(courseidbatch).get().then(async (doc) => {
    console.log(doc.data().startdate)
    for (var i = 0; i < doc.data().startdate.length; i++) {
        console.log(doc.data().startdate[i].batch)
        document.getElementById("batchdatanew").innerHTML += `
            <tr>
                <td>${doc.data().startdate[i].batch}</td>
                <td>${doc.data().startdate[i].date}</td>
                <td>${doc.data().startdate[i].time}</td>
                <td>${doc.data().startdate[i].noof}</td>
                <td>
                    <button class="btn btn-danger" id="${doc.data().startdate[i].batch}" onclick="removebatch(this)"><i class='fa fa-trash-o aria-hidden='true></i> Remove</button>
                    <span id="buttons${i}"></span>
                    <button class="btn btn-info" id="${doc.id}" data-bs-toggle='modal' data-bs-target='#exampleModaledit' onclick="edit(this.id)"><i class='fa fa-pencil aria-hidden='true></i> Edit</button>
               
                    </td>
            </tr>
       `
        if (doc.data().startdate[i].disable == true) {
            document.getElementById(`buttons${i}`).innerHTML = `<button class="btn btn-primary" id="${doc.data().startdate[i].batch}"onclick="unlock(this)">Unlock</button>`
        } else {
            document.getElementById(`buttons${i}`).innerHTML = `<button class="btn btn-primary" id="${doc.data().startdate[i].batch}" onclick="disable(this)">Disable</button>`
        }
    }

})
unlock = (e) => {
    var courseid = sessionStorage.getItem("courseidbatch")
    db.collection("newcourse").doc(courseid).get().then(async (doc) => {
        var datalatest = [], alldata = []
        for (var i = 0; i < doc.data().startdate.length; i++) {
            if (doc.data().startdate[i].batch == e.id) {
                datalatest.push({
                    batch: doc.data().startdate[i].batch,
                    date: doc.data().startdate[i].date,
                    time: doc.data().startdate[i].time,
                    noof: doc.data().startdate[i].noof,
                    disable: false
                })
            } else {
                alldata.push(doc.data().startdate[i])
            }
        }
        alldata.push(datalatest[0])
        db.collection("newcourse").doc(courseid).update({
            startdate: alldata

        }).then((doc) => {
            toastr["success"]("Course Updated Successfully..");
            setTimeout(() => { window.location.reload() }, 1000)
            // document.getElementById("enewcoursebtn").innerHTML = "Update"
        })
    })
}
disable = (e) => {
    var courseid = sessionStorage.getItem("courseidbatch")
    db.collection("newcourse").doc(courseid).get().then(async (doc) => {
        var datalatest = [], alldata = []
        for (var i = 0; i < doc.data().startdate.length; i++) {
            if (doc.data().startdate[i].batch == e.id) {
                datalatest.push({
                    batch: doc.data().startdate[i].batch,
                    date: doc.data().startdate[i].date,
                    time: doc.data().startdate[i].time,
                    noof: doc.data().startdate[i].noof,
                    disable: true
                })
            } else {
                alldata.push(doc.data().startdate[i])
            }
        }
        alldata.push(datalatest[0])
        db.collection("newcourse").doc(courseid).update({
            startdate: alldata

        }).then((doc) => {
            toastr["success"]("Course Updated Successfully..");
            setTimeout(() => { window.location.reload() }, 1000)
            // document.getElementById("enewcoursebtn").innerHTML = "Update"
        })
    })
}

removebatch = (e) => {
    var courseid = sessionStorage.getItem("courseidbatch")

    db.collection("newcourse").doc(courseid).get().then(async (doc) => {
        var datalatest = []
        for (var i = 0; i < doc.data().startdate.length; i++) {
            if (doc.data().startdate[i].batch == e.id) {
                doc.data().startdate.splice(i, 1);
            } else {
                datalatest.push(doc.data().startdate[i])
            }
        }
        db.collection("newcourse").doc(courseid).update({
            startdate: datalatest,
        }).then((doc) => {
            toastr["success"]("Batch Updated Successfully..");
            setTimeout(() => { window.location.reload() }, 1000)
            // document.getElementById("enewcoursebtn").innerHTML = "Update"
        })
    })


}

edit = (e) => {
    console.log(e.id)
    document.getElementById("startdatediv").innerHTML = ""
    document.getElementById("starttimediv").innerHTML = ""
    document.getElementById("startnoofdiv").innerHTML = ""
    document.getElementById("startbatchfdiv").innerHTML = ""

    db.collection("newcourse").doc(e).get().then(async (doc) => {
        for (var i = 0; i < doc.data().startdate.length; i++) {
            console.log(doc.data().startdate[i], doc.data().startdate[i].time)
            document.getElementById("startdatediv").innerHTML += `
            <input type="date" class="form-control usestartdate m-1" id="ustartdate" value="${doc.data().startdate[i].date}" placeholder="">
            `
            document.getElementById("starttimediv").innerHTML += `
            <input type="time" class="form-control usestarttime m-1" id="ustarttime" value="${doc.data().startdate[i].time}" placeholder="">
            `
            document.getElementById("startnoofdiv").innerHTML += `
            <input type="number" class="form-control usestartnoof m-1" id="ustartnoof" value="${doc.data().startdate[i].noof}" placeholder="">
            `
            document.getElementById("startbatchfdiv").innerHTML += `
            <input type="number" class="form-control usestartbatchf m-1" id="ustartnoof" value="${doc.data().startdate[i].batch}" placeholder="">
            `

        }
        document.getElementById("newcoursebtnu").addEventListener("click", async () => {
            document.getElementById("newcoursebtnu").innerHTML = " <span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span>Uploading..."

            var startdate = document.querySelectorAll(".usestartdate")
            var datadate = []
            for (var i = 0; i < startdate.length; i++) {
                var editdate = document.getElementsByClassName("usestartdate")[i].value
                var edittime = document.getElementsByClassName("usestarttime")[i].value
                var editnoo = document.getElementsByClassName("usestartnoof")[i].value
                var editbatch = document.getElementsByClassName("usestartbatchf")[i].value
                datadate.push({ batch: editbatch, date: editdate, time: edittime, noof: editnoo })
            }
            db.collection("newcourse").doc(e).update({
                startdate: datadate

            }).then((doc) => {
                toastr["success"]("Course Updated Successfully..");
                setTimeout(() => { window.location.reload() }, 1000)
                // document.getElementById("enewcoursebtn").innerHTML = "Update"
            })


        })
    })



}