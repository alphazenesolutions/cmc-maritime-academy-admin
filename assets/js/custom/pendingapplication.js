var db = firebase.firestore();
var pendingusers = [];
var allusersdata = [];
allusers();
async function allusers() {
  var userdetails = new Promise(async (resolve, reject) => {
    await db
      .collection("users")
      .get()
      .then((snap) => {
        var users = [];
        snap.forEach((doc) => {
          if (doc.data() !== undefined) {
            users.push(doc.data());
          }
        });
        return resolve(users);
      });
  });
  var purchasedetails = new Promise(async (resolve, reject) => {
    await db
      .collection("cart")
      .get()
      .then((snap) => {
        var order = [];
        snap.forEach((doc) => {
          if (doc.data() !== undefined) {
            order.push({
              data: doc.data(),
              id: doc.id,
            });
          }
        });
        return resolve(order);
      });
  });
  const user_details = await userdetails;
  const purchase_details = await purchasedetails;
  var pendinguser = [],
    userdata = [];

  for (var i = 0; i < purchase_details.length; i++) {
    for (var j = 0; j < user_details.length; j++) {
      if (purchase_details[i].data.userid == user_details[j].clientid) {
        var today = moment().format("YYYY-MM-DD");
        if (purchase_details[i].data.startdate >= today) {
          pendinguser.push(purchase_details[i].data);
          userdata.push(user_details[j]);
          document.getElementById("users").innerHTML += `
                        <tr>
                        <td><a href="javascript:void(0)" id="${user_details[j].clientid}" onclick="getuser(this)" style="cursor:pointer">${user_details[j].firstname} ${user_details[j].lastname}</a></td>
                           <td>${user_details[j].indosnumber}</td>
                        <td class="d-none d-md-table-cell">${purchase_details[i].data.title}</td>
                            <td class="ongoing">${user_details[j].phonenumber}</td>
                            <td class="ongoing">${purchase_details[i].data.startdate}</td>
                            <td>Batch - ${purchase_details[i].data.batch}</td>
                            <td class="ongoing">Not Purchaseing</td>
                            <td class="ongoing"><button class="btn btn-danger" id="${purchase_details[i].id}" onclick="deletepending(this)">Delete</button></td>
                        </tr>
                        `;
        }
      }
    }
  }
  pendingusers.push(...pendinguser);
  allusersdata.push(...userdata);
  console.log(pendinguser.length);
  if (pendinguser.length != 0) {
    document.getElementById("facultynoti").style.display = "none";
  }
  if (pendinguser.length != 0) {
    if (pendinguser.length > 25) {
      var items = $("#users .newcourse1");
      var numItems = pendinguser.length;
      var perPage = 25;
      items.slice(perPage).hide();
      $("#pagination-container1").pagination({
        items: numItems,
        itemsOnPage: perPage,
        prevText: "&laquo;",
        nextText: "&raquo;",
        onPageClick: function (pageNumber) {
          var showFrom = perPage * (pageNumber - 1);
          var showTo = showFrom + perPage;
          items.hide().slice(showFrom, showTo).show();
        },
      });
      document.getElementById("facultynoti").style.display = "none";
    }
  }
}
document.getElementById("searchname").addEventListener("keyup", async () => {
  document.getElementById("users").innerHTML = "";
  var searchname = document.getElementById("searchname").value.toLowerCase();
  var filterdoctor = [];
  for (var i = 0; i < allusersdata.length; i++) {
    if (
      allusersdata[i].indosnumber.toLowerCase().includes(searchname) ||
      allusersdata[i].phonenumber.toLowerCase().includes(searchname)
    ) {
      filterdoctor.push(allusersdata[i]);
    }
  }
  if (filterdoctor.length == 0) {
    document.getElementById("facultynoti").style.display = "block";
  }
  for (var i = 0; i < filterdoctor.length; i++) {
    for (var j = 0; j < pendingusers.length; j++) {
      if (pendingusers[j].userid == filterdoctor[i].clientid) {
        console.log(filterdoctor[i]);
        document.getElementById("users").innerHTML +=
          "<tr class='newcourse1'><td>" +
          filterdoctor[i].firstname +
          " " +
          filterdoctor[i].lastname +
          "</td><td>" +
          pendingusers[j].title +
          "</td><td>" +
          filterdoctor[i].phonenumber +
          "</td><td>" +
          pendingusers[j].startdate +
          "</td><td>";
      }
    }
  }
});
getuser = (e) => {
  console.log(e.id);
  localStorage.setItem("useridview", e.id);
  window.location.replace("/usersdisplay");
};

deletepending = (e) => {
  db.collection("cart")
    .doc(e.id)
    .delete()
    .then(() => {
      alert("Deleted..");
      setTimeout(() => {
        window.location.reload();
      }, 1000);
    });
};
