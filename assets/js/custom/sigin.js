var provider = new firebase.auth.GoogleAuthProvider();
var db = firebase.firestore()
document.getElementById("sigin").addEventListener("click", function (e) {
    e.preventDefault()
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var emilreg = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
    if (!emilreg.test(email)) {
        document.getElementById("fbemail").style.display = "block"
        return false
    } else if (password.length == 0) {
        document.getElementById("fbpassword").style.display = "block"
        return false
    } else {
        document.getElementById("sigin").innerHTML = " <span class='spinner-border spinner-border-sm mr-2' role='status'style='padding: 7px;' aria-hidden='true'></span>Please Wait..."
        db.collection("admin").doc(email).get().then((doc) => {
            if (doc.data() != undefined) {
                if (password == doc.data().password) {
                    toastr["success"]("Welcome to Admin Panel")
                    localStorage.setItem("userid", doc.data().userid)
                    localStorage.setItem("role", doc.data().role)
                    if (doc.data().role == "admin") {
                        setTimeout(() => {
                            window.location.replace("/home")
                        }, 2000)
                    } else {
                        setTimeout(() => {
                            window.location.replace("/acchome")
                        }, 2000)
                    }
                } else {
                    toastr["error"]("Please Enter The Correct Password")
                }
            }
            else {
                toastr["error"]("Please Check Email And Password..")
            }


        })

    }

})

var userid = localStorage.getItem("userid")
var role = localStorage.getItem("role")
if (userid != undefined) {
    if (role == "admin") {
        window.location.replace("/home")
    } else {
        window.location.replace("/acchome")
    }

} else {
    // toastr["error"]("Please Login..");
}