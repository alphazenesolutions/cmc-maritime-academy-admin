var db = firebase.firestore();

db.collection("users")
    .orderBy("date", "desc").get().then((snap) => {
        data = []
        snap.forEach((doc) => {
            data.push(doc.data())
            document.getElementById("users").innerHTML += `
                <tr class='newcourse1'>
                <td><a href="javascript:void(0)" id="${doc.data().clientid}" onclick="getuser(this)" style="cursor:pointer">${doc.data().firstname} ${doc.data().lastname}</a></td>
                    <td>${doc.data().indosnumber}</td>
                    <td>${doc.data().email}</td>
                    <td>${doc.data().phonenumber}</td>
                    <td>${doc.data().passport == undefined ? "-" : doc.data().passport}</td>
                    <td>${doc.data().dob}</td>
                </tr>
                `
        })

        if (data.length != 0) {
            var items = $("#users .newcourse1");
            var numItems = data.length;
            var perPage = 25;
            items.slice(perPage).hide();
            $('#pagination-container1').pagination({
                items: numItems,
                itemsOnPage: perPage,
                prevText: "&laquo;",
                nextText: "&raquo;",
                onPageClick: function (pageNumber) {
                    var showFrom = perPage * (pageNumber - 1);
                    var showTo = showFrom + perPage;
                    items.hide().slice(showFrom, showTo).show();
                }
            });
            document.getElementById("facultynoti").style.display = "none"
        } else {
            document.getElementById("facultynoti").style.display = "block"
        }
    })

document.getElementById("usersearch").addEventListener("keyup", async () => {
    document.getElementById("users").innerHTML = ""
    var searchname = document.getElementById("usersearch").value.toLowerCase()
    db.collection("users")
        .orderBy("date", "desc").get().then((snap) => {
            var filterdoctor = []
            snap.forEach((doc) => {
                if (doc.data().indosnumber.toLowerCase().includes(searchname) || doc.data().phonenumber.toLowerCase().includes(searchname)) {
                    filterdoctor.push(doc.data())
                }
            })

            for (var i = 0; i < filterdoctor.length; i++) {
                document.getElementById("users").innerHTML += `
            <tr class='newcourse1'>
            <td><a href="javascript:void(0)" id="${filterdoctor.clientid}" onclick="getuser(this)" style="cursor:pointer">${filterdoctor.firstname} ${filterdoctor.lastname}</a></td>
                <td>${filterdoctor[i].indosnumber}</td>
                <td>${filterdoctor[i].email}</td>
                <td>${filterdoctor[i].phonenumber}</td>
                <td>${filterdoctor[i].passport == undefined ? "-" : filterdoctor[i].passport}</td>
                <td>${filterdoctor[i].dob}</td>
            </tr>
            `
            }
            if (filterdoctor.length == 0) {
                document.getElementById("facultynoti").style.display = "block"
            }
        })



})
getuser = (e) => {
    console.log(e.id)
    localStorage.setItem("useridview", e.id)
    window.location.replace("/usersdisplay")
}