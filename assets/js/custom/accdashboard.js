var db = firebase.firestore();
allusers()
async function allusers() {
    await db.collection("users").get().then((snap) => {
        var users = []
        snap.forEach((doc) => {
            if (doc.data() !== undefined) {
                users.push(doc.data())
            }
        })
        document.getElementById("customers").innerHTML = users.length
    })
}

allcourses()
async function allcourses() {
    await db.collection("newcourse").get().then((snap) => {
        var allcourses = []
        snap.forEach((doc) => {
            if (doc.data() !== undefined) {
                allcourses.push(doc.data())
            }
        })
        document.getElementById("courses").innerHTML = allcourses.length
    })
}

allcategory()
async function allcategory() {
    await db.collection("category").get().then((snap) => {
        var allcategory = []
        snap.forEach((doc) => {
            if (doc.data() !== undefined) {
                allcategory.push(doc.data())
            }
        })
        document.getElementById("category").innerHTML = allcategory.length
    })
}

purchasecourses()
async function purchasecourses() {
    var today = moment().format("YYYY-MM-DD")
    var month = moment().format("MM")
    var year = moment().format("YYYY")
    var startOfweek = startOfWeek(new Date())
    var startOfWeeks = moment(startOfweek).format("YYYY-MM-DD")
    var previousweek=moment(startOfWeeks).subtract(7,"days").format("YYYY-MM-DD")
    await db.collection("purchasecourses").get().then(async(snap) => {
        var purchasecourses = []
        snap.forEach((doc) => {
            if (doc.data() !== undefined) {
                var purchasecourse = doc.data()
                var date = doc.data().date.split(" ")[0]
                purchasecourse["date"] = date
                purchasecourses.push(purchasecourse)
            }
        })
        document.getElementById("phtn").innerHTML = purchasecourses.length
        var thisYear = [], thisMonth = [], thisweek = [],thisday=[],prevweek=[]
        for (var i = 0; i < purchasecourses.length; i++) {
            var purchaseYear = purchasecourses[i].date.split("-")[0]
            var purchaseMonth = purchasecourses[i].date.split("-")[1]
            if (purchaseYear == year) {
                thisYear.push(purchasecourses[i].amount)
            }
            if (purchaseMonth == month) {
                thisMonth.push(purchasecourses[i].amount)
            }
            if ((purchasecourses[i].date >= startOfWeeks) && (purchasecourses[i].date <= today)) {
                thisweek.push(purchasecourses[i].amount)
            }
            if ((purchasecourses[i].date >= previousweek) && (purchasecourses[i].date <= startOfWeeks)) {
                prevweek.push(purchasecourses[i].amount)
            }
            if ((purchasecourses[i].date == today) ) {
                thisday.push(purchasecourses[i].amount)
            }
        }
        document.getElementById("phmonth").innerHTML = thisMonth.length
        document.getElementById("phyear").innerHTML = thisYear.length
        document.getElementById("phweek").innerHTML = thisweek.length
        var thisMonthamount = await thisMonth.reduce((a, b) => Number(a) + Number(b), 0)
        document.getElementById("rvmonth").innerHTML = `₹ ${thisMonthamount}`
        var thisweekamount = await thisweek.reduce((a, b) => { return Number(a) + Number(b) }, 0)
        document.getElementById("rvweek").innerHTML = `₹ ${thisweekamount}`
        var thisdayamount = await thisday.reduce((a, b) => { return Number(a) + Number(b) }, 0)
        document.getElementById("rvtd").innerHTML = `₹ ${thisdayamount}`
        var prevweekamount = await thisYear.reduce((a, b) => { return Number(a) + Number(b) }, 0)
        document.getElementById("rvpweek").innerHTML = `₹ ${prevweekamount}`
    })

}
function startOfWeek(date) {
    var diff = date.getDate() - date.getDay() + (date.getDay() === 0 ? -6 : 1);
    return new Date(date.setDate(diff));
}
