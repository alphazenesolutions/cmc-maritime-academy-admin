var db = firebase.firestore();
document.getElementById("newccatbtn").addEventListener("click", async () => {

    var email = document.getElementById("email").value
    var name = document.getElementById("name").value
    var phone = document.getElementById("phone").value
    var password = document.getElementById("password").value
    var cpassword = document.getElementById("cpassword").value
    var profile = document.querySelector("#profile").files

    if (cpassword == password) {
        document.getElementById("newccatbtn").innerHTML = " <span class='spinner-border spinner-border-sm mr-2' role='status'style='padding: 7px;' aria-hidden='true'></span>Please Wait..."
        let file11 = new Promise((resolve, reject) => {
            var storageRef = firebase.storage().ref("profile/" + profile[0].name);
            storageRef.put(profile[0]).then(function (snapshot) {
                storageRef.getDownloadURL().then(function (url) {  //img download link ah ketakiradhu
                    setTimeout(() => resolve(url), 1000)
                })
            });

        });
        var imgurl = await file11
        db.collection("admin").doc(email).set({
            email: email,
            name: name,
            phone: phone,
            password: password,
            role: "admin",
            profile: imgurl,
            userid: Date.now().toString()
        }).then(() => {
            toastr["success"]("New Admin Added Successfully..");
            setTimeout(() => { window.location.reload() }, 2000)
        }).catch((error) => {
            console.log(error.message)
            toastr["error"](error.message);
        })
    } else {
        toastr["error"]("Please Provide Same Password..");
    }
})

db.collection("admin").get().then((snap) => {
    var categorydata = []
    snap.forEach((doc) => {
        categorydata.push(doc.data())
        if (doc.data().role == "admin") {
            document.getElementById("category").innerHTML += `
        <tr>
            <td><a href="${doc.data().profile}" target=__blank><img src="${doc.data().profile}" style="width:50px;height:50px;"></a></td>
            <td>${doc.data().name}</td>
            <td>${doc.data().email}</td>
            <td>${doc.data().phone == undefined ? "-" : doc.data().phone}</td>
            <td><button class='btn btn-danger' id="${doc.data().email}" onclick='del(this.id)'><i class='fa fa-trash-o aria-hidden='true></i> Delete</button><button class='btn btn-info' data-toggle="modal"
            data-target="#editexampleModal" id="${doc.data().email}" onclick='edit(this.id)'><i class='fa fa-pencil aria-hidden='true></i> Edit</button></td>
        </tr>
        `

        }
    })


    if (categorydata.length != 0) {

        document.getElementById("coursenoti").style.display = "none"
    } else {
        document.getElementById("coursenoti").style.display = "block"
    }
})

del = (e) => {
    db.collection("admin").doc(e).delete().then(() => {
        toastr["error"]("Admin Deleted..");
        setTimeout(() => { window.location.reload() }, 1000)
    })

}

view = (e) => {
    db.collection("category").doc(e).get().then((doc) => {
        sessionStorage.setItem("categoryname", doc.data().category)
    })
    sessionStorage.setItem("categoryid", e)
    setTimeout(() => { window.location.replace("/newcourse") }, 1000)
}

edit = (e) => {
    db.collection("admin").doc(e).get().then((doc) => {
        document.getElementById("eemail").value = doc.data().email
        document.getElementById("ename").value = doc.data().name
        document.getElementById("ephone").value = doc.data().phone
        document.getElementById("epassword").value = doc.data().password
        document.getElementById("ecpassword").value = doc.data().password
    })
}

document.getElementById("updateadminbtn").addEventListener("click", async () => {
    var email = document.getElementById("eemail").value
    var name = document.getElementById("ename").value
    var phone = document.getElementById("ephone").value
    var password = document.getElementById("epassword").value
    var cpassword = document.getElementById("ecpassword").value
    var cprofile = document.querySelector("#eprofile").files

    if (cprofile.length == 0) {
        if (cpassword == password) {
            db.collection("admin").doc(email).update({
                email: email,
                name: name,
                phone: phone,
                password: password,
            }).then(() => {
                toastr["success"]("Admin Updated Successfully..");
                setTimeout(() => { window.location.reload() }, 2000)
            }).catch((error) => {
                console.log(error.message)
                toastr["error"](error.message);
            })
        } else {
            toastr["error"]("Please Provide Same Password..");
        }
    } else {
        if (cpassword == password) {
            let file11 = new Promise((resolve, reject) => {
                var storageRef = firebase.storage().ref("profile/" + cprofile[0].name);
                var test = [];
                storageRef.put(cprofile[0]).then(function (snapshot) {
                    storageRef.getDownloadURL().then(function (url) {  //img download link ah ketakiradhu
                        setTimeout(() => resolve(url), 1000)
                    })
                });

            });
            var imgurl = await file11
            db.collection("admin").doc(email).update({
                email: email,
                name: name,
                phone: phone,
                password: password,
                profile: imgurl,
            }).then(() => {
                toastr["success"]("Admin Updated Successfully..");
                setTimeout(() => { window.location.reload() }, 2000)
            }).catch((error) => {
                console.log(error.message)
                toastr["error"](error.message);
            })
        } else {
            toastr["error"]("Please Provide Same Password..");
        }
    }



})

document.getElementById("name").addEventListener("keyup", (e) => {
    var searchvalue = e.target.value
    db.collection("faculty").get().then((snap) => {
        facultydata = []
        snap.forEach((doc) => {
            if (doc.data() != undefined) {
                if (doc.data().facultyid == undefined) {
                    facultydata.push(doc.data())
                }
            }
        })
        for (var i = 0; i < facultydata.length; i++) {
            var finalservice = []
            if (facultydata[i].fname.toLowerCase().includes(searchvalue.toLowerCase())) {
                finalservice.push(facultydata[i])
            }
            for (var j = 0; j < finalservice.length; j++) {
                document.getElementById("facultyname").innerHTML = ""
                document.getElementById("facultyname").innerHTML += `
                <span class="pcoded-mtext" id="${finalservice[j].fname}" onclick="facultynamebtn(this)">${finalservice[j].fname}</span>
                                        `
            }





        }
    })


})

facultynamebtn = (e) => {
    document.getElementById("name").value = e.id
    document.getElementById("facultyname").innerHTML = ""
}


document.getElementById("usersearch").addEventListener("keyup", async () => {
    document.getElementById("category").innerHTML = ""
    var searchname = document.getElementById("usersearch").value.toLowerCase()
    db.collection("admin").get().then((snap) => {
            var filterdoctor = []
            snap.forEach((doc) => {
                if (doc.data().name.toLowerCase().includes(searchname) ) {
                    filterdoctor.push(doc.data())
                }
            })
           for(var i=0;i<filterdoctor.length;i++){
               document.getElementById("category").innerHTML += `
            <tr>
                <td><img src="${filterdoctor[i].profile}" style="width:50px;height:50px;"></td>
                <td>${filterdoctor[i].name}</td>
                <td>${filterdoctor[i].email}</td>
                <td>${filterdoctor[i].phone}</td>
                <td><button class='btn btn-danger' disabled id="${filterdoctor[i]}" onclick='del(this.id)'><i class='fa fa-trash-o aria-hidden='true></i> Delete</button> <button disabled class='btn btn-info' id="${filterdoctor[i]}" data-bs-toggle='modal' data-bs-target='#exampleModaledit' onclick='edit(this.id)'><i class='fa fa-pencil aria-hidden='true></i> Edit</button></td>
            </tr>
            `
           }
        })

       
})