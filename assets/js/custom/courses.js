var db = firebase.firestore();

var categoryid = sessionStorage.getItem("categoryid")
var categoryname = sessionStorage.getItem("categoryname")
db.collection("category").get().then((snap) => {
    snap.forEach((doc) => {
        if (doc.data().category !== "All") {
            document.getElementById("category").innerHTML += `<option  value="${doc.data().category}">${doc.data().category}</option>`
        }

    })
})

document.getElementById("newcoursebtn").addEventListener("click", async () => {

    var tabledata = []
    var data = {}
    var batch = document.querySelectorAll(".batch")
    for (var i = 0; i < batch.length; i++) {
        tabledata.push(
            data = {
                "batch": document.getElementsByClassName("batch")[i].value,
                "date": document.getElementsByClassName("date")[i].value,
                "time": document.getElementsByClassName("time")[i].value,
                "noof": document.getElementsByClassName("noof")[i].value,
                "booked": 0
            }
        )
    }

    var category = document.getElementById("category").value
    var title = document.getElementById("title").value
    var subheading = document.getElementById("subheading").value
    var duration = document.getElementById("duration").value
    var mode = document.getElementById("mode").value
    var rfees = document.getElementById("rfees").value
    var ofees = document.getElementById("ofees").value
    var eligibility = document.getElementById("eligibility").value
    var documents = document.getElementById("documents").value
    var description = document.getElementById("description").value
    var thumb = document.querySelector("#thumb").files
    document.getElementById("newcoursebtn").innerHTML = " <span class='spinner-border spinner-border-sm mr-2' role='status'style='padding: 7px;' aria-hidden='true'></span>Please Wait..."
    let file11 = new Promise((resolve, reject) => {
        var storageRef = firebase.storage().ref("thumbnail/" + thumb[0].name);
        var test = [];
        storageRef.put(thumb[0]).then(function (snapshot) {
            storageRef.getDownloadURL().then(function (url) {  //img download link ah ketakiradhu
                setTimeout(() => resolve(url), 1000)
            })
        });

    });
    var imgurl = await file11
    const today = new Date();
    const monthsandday = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40",
        "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60"]
    const date = today.getFullYear() + '-' + (monthsandday[today.getMonth() + 1]) + '-' + (monthsandday[today.getDate()]);
    const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    const dateTime = date + ' ' + time;
    var newcoursedata = {
        category: category,
        title: title,
        subheading: subheading,
        duration: duration,
        mode: mode,
        rfees: rfees,
        ofees: ofees,
        eligibility: eligibility.trim().length == 0 ? "Nill" : eligibility,
        documents: documents.trim().length == 0 ? "Nill" : documents,
        description: description.trim().length == 0 ? "Nill" : description,
        thum: imgurl,
        date: dateTime,
        startdate: tabledata,
    }
    console.log(newcoursedata)

    db.collection("newcourse").doc().set(newcoursedata).then(() => {

        toastr["success"]("New Course Created Successfully..");
        setTimeout(() => { window.location.reload() }, 2000)
    }).catch((error) => {
        console.log(error.message)
        toastr["error"](error.message);
    })
})


managebatch = (e) => {
    sessionStorage.setItem("courseidbatch", e)
    window.location.replace("/managebatch")
    // document.getElementById("newbatchaddbtn").addEventListener("click", async () => {
    //     var tabledata = []
    //     var data = {}
    //     tabledata.push(
    //         data = {
    //             "batch": document.getElementById("batchnew").value,
    //             "date": document.getElementById("datenew").value,
    //             "time": document.getElementById("timenew").value,
    //             "noof": document.getElementById("noofnew").value,

    //         }
    //     )
    //     var finaldata = []
    //     await db.collection("newcourse").doc(e).get().then(async (doc) => {
    //         var datalatest = []
    //         for (var i = 0; i < doc.data().startdate.length; i++) {
    //             finaldata.push(doc.data().startdate[i])
    //         }
    //         if (datalatest.length != 0) {
    //             finaldata.push(datalatest)
    //         }
    //     })
    //     finaldata.push(tabledata[0])
    //     db.collection("newcourse").doc(e).update({
    //         startdate: finaldata,
    //     }).then((doc) => {
    //         toastr["success"]("Batch Updated Successfully..");
    //         setTimeout(() => { window.location.reload() }, 1000)
    //         // document.getElementById("enewcoursebtn").innerHTML = "Update"
    //     })
    // })

    // db.collection("newcourse").doc(e).get().then(async (doc) => {
    //     for (var i = 0; i < doc.data().startdate.length; i++) {
    //         document.getElementById("batchdata").innerHTML += `
    //             <tr>
    //                 <td>${doc.data().startdate[i].batch}</td>
    //                 <td>${doc.data().startdate[i].date}</td>
    //                 <td>${doc.data().startdate[i].time}</td>
    //                 <td>${doc.data().startdate[i].noof}</td>
    //                 <td><button class="btn btn-danger" id="${doc.data().startdate[i].batch}" onclick="removebatch(this)">Remove</button></td>
    //             </tr>
    //        `
    //     }

    // })
}

removebatch = (e) => {
    var courseid = sessionStorage.getItem("courseidbatch")
    console.log(e.id)
    db.collection("newcourse").doc(courseid).get().then(async (doc) => {
        var datalatest = []
        for (var i = 0; i < doc.data().startdate.length; i++) {
            if (doc.data().startdate[i].batch == e.id) {
                doc.data().startdate.splice(i, 1);
            } else {
                datalatest.push(doc.data().startdate[i])
            }
        }
        console.log(datalatest)
        db.collection("newcourse").doc(courseid).update({
            startdate: datalatest,
        }).then((doc) => {
            toastr["success"]("Batch Updated Successfully..");
            setTimeout(() => { window.location.reload() }, 1000)
            // document.getElementById("enewcoursebtn").innerHTML = "Update"
        })
    })


}



db.collection("newcourse").orderBy("date", "desc").get().then((snap) => {
    coursedata = []
    snap.forEach((doc) => {
        if (doc.data() != undefined) {
            coursedata.push({
                data: doc.data(),
                id: doc.id
            })

        } else {
            document.getElementById("coursenoti").style.display = "block"
        }
    })
    var data = coursedata.sort((a, b) => {
        if (a.data.title > b.data.title) return 1;
        if (a.data.title < b.data.title) return -1;
        return 0;
    });
    for (var i = 0; i < data.length; i++) {
        document.getElementById("coursenoti").style.display = "none"
        document.getElementById("newcourse").innerHTML += "<tr class='coursedata1'><td>" + data[i].data.title + "</td><td>" + data[i].data.category + "</td><td>" + data[i].data.duration + "</td><td>" + data[i].data.ofees
            + "</td><td>" + "<button class='btn btn-danger' id=" + data[i].id + " onclick='del(this.id)'><i class='fa fa-trash-o aria-hidden='true></i> Delete</button> <button class='btn btn-info' id=" + data[i].id + " data-bs-toggle='modal' data-bs-target='#exampleModaledit' onclick='edit(this.id)'><i class='fa fa-pencil aria-hidden='true></i> Edit</button><button class='btn btn-primary' id=" + data[i].id + " data-bs-toggle='modal' data-bs-target='#exampleModalmanagebatch' onclick='managebatch(this.id)'><i class='fa fa-eye aria-hidden='true></i> View Batch</button>" + "</td></tr>"
    }
    if (coursedata.length != 0) {
        if (coursedata.length > 15) {
            var items = $(".newcourse .coursedata1");
            var numItems = coursedata.length;
            var perPage = 15;
            items.slice(perPage).hide();
            $('#pagination-containerf1').pagination({
                items: numItems,
                itemsOnPage: perPage,
                prevText: "&laquo;",
                nextText: "&raquo;",
                onPageClick: function (pageNumber) {
                    var showFrom = perPage * (pageNumber - 1);
                    var showTo = showFrom + perPage;
                    items.hide().slice(showFrom, showTo).show();
                }
            });
        }
        document.getElementById("coursenoti").style.display = "none"
    } else {
        document.getElementById("coursenoti").style.display = "block"
    }
})

document.getElementById("usersearch").addEventListener("keyup", async () => {
    document.getElementById("newcourse").innerHTML = ""
    var searchname = document.getElementById("usersearch").value.toLowerCase()
    db.collection("newcourse").get().then((snap) => {
        var filterdoctor = []
        snap.forEach((doc) => {
            if (doc.data().title.toLowerCase().includes(searchname)) {
                filterdoctor.push(doc.data())
            }
        })
        for (var i = 0; i < filterdoctor.length; i++) {

            document.getElementById("newcourse").innerHTML += "<tr class='newcourse1'><td>" + filterdoctor[i].title + "</td><td>" + filterdoctor[i].category + "</td><td>" + filterdoctor[i].startdate[i].date + "</td><td>" + filterdoctor[i].startdate[i].batch + "</td><td>" + filterdoctor[i].duration + "</td><td>" + filterdoctor[i].ofees
                + "</td><td>" + "<button class='btn btn-danger' id=" + filterdoctor[i].id + " onclick='del(this.id)'><i class='fa fa-trash-o aria-hidden='true></i> Delete</button> <button class='btn btn-info' id=" + filterdoctor[i].id + " data-bs-toggle='modal' data-bs-target='#exampleModaledit' onclick='edit(this.id)'><i class='fa fa-pencil aria-hidden='true></i> Edit</button>" + "</td></tr>"
        }


    })
})



del = (e) => {

    var alertdata = confirm("Are You Sure Delete Course..");
    if (alertdata === true) {
        db.collection("newcourse").doc(e).delete().then(() => {
            // toastr["error"]("Course Deleted");
            setTimeout(() => { window.location.reload() }, 1000)
        })
    }
}


edit = (e) => {
    document.getElementById("startdatediv").innerHTML = ""
    document.getElementById("starttimediv").innerHTML = ""
    document.getElementById("startnoofdiv").innerHTML = ""
    document.getElementById("startbatchfdiv").innerHTML = ""
    console.log(e)
    db.collection("newcourse").doc(e).get().then(async (doc) => {
        console.log(doc.data())
        document.getElementById("ucategory").value = doc.data().category
        document.getElementById("udescription").value = doc.data().description
        document.getElementById("udocuments").value = doc.data().documents
        document.getElementById("uduration").value = doc.data().duration
        document.getElementById("ueligibility").value = doc.data().eligibility
        document.getElementById("umode").value = doc.data().mode
        document.getElementById("uofees").value = doc.data().ofees
        document.getElementById("urfees").value = doc.data().rfees
        document.getElementById("usubheading").value = doc.data().subheading
        document.getElementById("utitle").value = doc.data().title
        for (var i = 0; i < doc.data().startdate.length; i++) {
            console.log(doc.data().startdate[i], doc.data().startdate[i].time)
            document.getElementById("startdatediv").innerHTML += `
            <input type="date" class="form-control usestartdate" id="ustartdate" value="${doc.data().startdate[i].date}" placeholder="">
            `
            document.getElementById("starttimediv").innerHTML += `
            <input type="time" class="form-control usestarttime" id="ustarttime" value="${doc.data().startdate[i].time}" placeholder="">
            `
            document.getElementById("startnoofdiv").innerHTML += `
            <input type="number" class="form-control usestartnoof" id="ustartnoof" value="${doc.data().startdate[i].noof}" placeholder="">
            `
            document.getElementById("startbatchfdiv").innerHTML += `
            <input type="number" class="form-control usestartbatchf" id="ustartnoof" value="${doc.data().startdate[i].batch}" placeholder="">
            `

        }
        document.getElementById("newcoursebtnu").addEventListener("click", async () => {
            var thumb = document.querySelector("#uthumb").files
            if (thumb.length == 0) {
                document.getElementById("newcoursebtnu").innerHTML = " <span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span>Uploading..."

                var startdate = document.querySelectorAll(".usestartdate")
                var datadate = []
                for (var i = 0; i < startdate.length; i++) {
                    var editdate = document.getElementsByClassName("usestartdate")[i].value
                    var edittime = document.getElementsByClassName("usestarttime")[i].value
                    var editnoo = document.getElementsByClassName("usestartnoof")[i].value
                    var editbatch = document.getElementsByClassName("usestartbatchf")[i].value
                    datadate.push({ batch: editbatch, date: editdate, time: edittime, noof: editnoo })
                }
                db.collection("newcourse").doc(e).update({
                    category: document.getElementById("ucategory").value,
                    description: document.getElementById("udescription").value,
                    documents: document.getElementById("udocuments").value,
                    duration: document.getElementById("uduration").value,
                    eligibility: document.getElementById("ueligibility").value,
                    mode: document.getElementById("umode").value,
                    ofees: document.getElementById("uofees").value,
                    rfees: document.getElementById("urfees").value,
                    subheading: document.getElementById("usubheading").value,
                    title: document.getElementById("utitle").value,
                    startdate: datadate

                }).then((doc) => {
                    toastr["success"]("Course Updated Successfully..");
                    setTimeout(() => { window.location.reload() }, 1000)
                    // document.getElementById("enewcoursebtn").innerHTML = "Update"
                })
            } else {
                let file11 = new Promise((resolve, reject) => {
                    var storageRef = firebase.storage().ref("thumbnail/" + thumb[0].name);
                    var test = [];
                    storageRef.put(thumb[0]).then(function (snapshot) {
                        storageRef.getDownloadURL().then(function (url) {  //img download link ah ketakiradhu
                            setTimeout(() => resolve(url), 1000)
                        })
                    });

                });
                var imgurl = await file11
                document.getElementById("newcoursebtnu").innerHTML = " <span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span>Uploading..."

                var startdate = document.querySelectorAll(".usestartdate")
                var datadate = []
                for (var i = 0; i < startdate.length; i++) {
                    var editdate = document.getElementsByClassName("usestartdate")[i].value
                    var edittime = document.getElementsByClassName("usestarttime")[i].value
                    var editnoo = document.getElementsByClassName("usestartnoof")[i].value
                    var editbatch = document.getElementsByClassName("usestartbatchf")[i].value
                    datadate.push({ batch: editbatch, date: editdate, time: edittime, noof: editnoo })
                }
                db.collection("newcourse").doc(e).update({
                    category: document.getElementById("ucategory").value,
                    description: document.getElementById("udescription").value,
                    documents: document.getElementById("udocuments").value,
                    duration: document.getElementById("uduration").value,
                    eligibility: document.getElementById("ueligibility").value,
                    mode: document.getElementById("umode").value,
                    ofees: document.getElementById("uofees").value,
                    rfees: document.getElementById("urfees").value,
                    subheading: document.getElementById("usubheading").value,
                    title: document.getElementById("utitle").value,
                    startdate: datadate,
                    thum: imgurl
                }).then((doc) => {
                    toastr["success"]("Course Updated Successfully..");
                    setTimeout(() => { window.location.reload() }, 1000)
                    // document.getElementById("enewcoursebtn").innerHTML = "Update"
                })
            }


        })
    })



}